package com.skysoft.skyemo;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

import com.skysoft.skyemo.utils.Constants;
import com.skysoft.skyemo.utils.PreferenceUtils;


public class PlayActivity extends ActionBarActivity implements Constants{

    TextView tvEmoText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Remove title bar
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_play);
        tvEmoText = (TextView) findViewById(R.id.tv_emotext);

        // get the text from save preferences then play
        String emoText = getIntent().getStringExtra(KEY_SAVED_STRING);
        if((emoText != null) && !emoText.isEmpty()) {
            tvEmoText.setText(emoText);
            tvEmoText.setTextSize(getResources().getDimension(R.dimen.text_size));
        } else {
            tvEmoText.setText(PreferenceUtils.getString(this, KEY_SAVED_STRING));
        }

        //Animation animation = AnimationUtils.loadAnimation(this, R.anim.vibrateanimation);

        TranslateAnimation animation = new TranslateAnimation(0.0f, 400.0f,
                0.0f, 0.0f);          //  new TranslateAnimation(xFrom,xTo, yFrom,yTo)
        animation.setDuration(5000);  // animation duration
        animation.setRepeatCount(5);  // animation repeat count
        animation.setRepeatMode(2);   // repeat animation (left to right, right to left )
        //animation.setFillAfter(true);
        tvEmoText.startAnimation(animation);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_play, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
