package com.skysoft.skyemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.skysoft.skyemo.utils.Constants;
import com.skysoft.skyemo.utils.PreferenceUtils;


public class MainActivity extends ActionBarActivity implements Constants, View.OnClickListener{

    private static final String TAG = "EmoActivity";
    private EditText emoName;
    private EditText emoText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        emoText = (EditText) findViewById(R.id.et_emo_content);
        emoName = (EditText) findViewById(R.id.et_emo_name);

        final Button buttonPreview = (Button) findViewById(R.id.btn_preview);
        buttonPreview.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                PreferenceUtils.saveString(this, KEY_SAVED_STRING, emoText.getText().toString());
                break;
            case R.id.btn_preview:
                Intent iPlay = new Intent(this, PlayActivity.class);
                iPlay.putExtra(KEY_SAVED_STRING, emoText.getText().toString());
                startActivity(iPlay);
                break;
        }
    }
}
