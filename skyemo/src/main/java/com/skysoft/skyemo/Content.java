package com.skysoft.skyemo;

import java.util.Date;

/**
 * Created by BinhHa on 3/29/2015.
 */
public class Content {

    private String emo;

    private Date createdDate;
    private Date modifiedDate;

    public String getEmo() {
        return emo;
    }

    public void setEmo(String emo) {
        this.emo = emo;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
}
