package com.skysoft.skyemo.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Ngọc Nguyễn on 3/31/2015.
 */
public class PreferenceUtils {

    public static void saveString(Context ctx, String key, String value) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(ctx);
        pref.edit().putString(key, value).commit();
    }

    public static String getString(Context ctx, String key) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(ctx);
        return pref.getString(key, "");
    }

}
